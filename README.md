# WinAutoPilot

Gathers a device's hardware hash for manual enrollment in Intune Auto Pilot.

Conponents for this scrip were obtained from Microsoft. For more details visit https://docs.microsoft.com/en-us/mem/autopilot/add-devices

# About this script

This script will create a folder in the C directory called HWID and will produce a file named "AutoPilotHWID-serialnumber.csv"

*This script must be ran with administrative privilges.